﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImicAspnetMvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.BatCuCaiGi = 1;
            return View();
        }

        public ActionResult Contact(int id2)
        {
            ViewBag.Id = id2;
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}