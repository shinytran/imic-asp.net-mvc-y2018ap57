﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ImicAspnetMvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id2}", //Home/about -> Controller: Home, Action: Contact
                defaults: new { controller = "Home", action = "Index", id2 = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default2",
                url: "home2-{action}/{controller}/{id}", //about/home-1 -> Controller: Home2, Action: About
                defaults: new { controller = "Home2", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
